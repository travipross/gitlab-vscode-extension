import { browser, expect } from '@wdio/globals';
import {
  dismissAllNotifications,
  waitForNotification,
  waitForPromptToContain,
} from '../helpers/index.js';

describe('GitLab Workflow Extension Authorization', () => {
  let prompt;
  let workbench;

  beforeEach(async () => {
    await dismissAllNotifications();
    workbench = await browser.getWorkbench();
    prompt = await workbench.executeCommand('GitLab: Add Account to VS Code');
  });

  it('fails to authenticate with an invalid token', async () => {
    await waitForPromptToContain(prompt, 'URL to Gitlab instance');
    await prompt.confirm();

    await prompt.setText('INVALID_TOKEN');
    await prompt.confirm();

    await waitForNotification('API Unauthorized');
  });

  it('authenticates with a valid token', async () => {
    await waitForPromptToContain(prompt, 'URL to Gitlab instance');
    await prompt.confirm();

    await expect(prompt.input$).toHaveAttr('type', 'password', {
      message: 'Input field is not masked for passwords.',
    });

    await prompt.setText(process.env.TEST_GITLAB_TOKEN);
    await prompt.confirm();

    await waitForNotification('Added the GitLab account for user');
  });
});
