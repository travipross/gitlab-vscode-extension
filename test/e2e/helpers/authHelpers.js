import { browser } from '@wdio/globals';
import { waitForPromptToContain } from './commandPaletteHelpers.js';
import { dismissAllNotifications, waitForNotification } from './notificationHelpers.js';

/**
 * Completes authorization with environment variable `TEST_GITLAB_TOKEN` for the GitLab extension
 *
 * @async
 * @returns {Promise<void>}
 */
const completeAuth = async () => {
  if (!process.env.TEST_GITLAB_TOKEN) {
    throw new Error('TEST_GITLAB_TOKEN environment variable is not set!');
  }

  await dismissAllNotifications();
  const workbench = await browser.getWorkbench();
  const prompt = await workbench.executeCommand('GitLab: Add Account to VS Code');

  await waitForPromptToContain(prompt, 'URL to Gitlab instance');
  await prompt.confirm();

  await expect(prompt.input$).toHaveAttr('type', 'password', {
    message: 'Input field is not masked for passwords.',
  });

  await prompt.setText(process.env.TEST_GITLAB_TOKEN);
  await prompt.confirm();

  await waitForNotification('Added the GitLab account for user');
};

export { completeAuth };
